package com.wxbus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 配置 web MVC 的方法
 * Created by g1154 on 2018/5/12.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

    //  registry.addViewController("/web/index").setViewName("index");
        registry.addViewController("/web/login").setViewName("login");
        registry.addViewController("/web/bus").setViewName("bus");
        registry.addViewController("/web/add/bus").setViewName("addbus");
        registry.addViewController("/web/bus/allot").setViewName("allot");
        registry.addViewController("/web/station").setViewName("siteinfor");
        registry.addViewController("/web/add/station").setViewName("addsite");
        registry.addViewController("/web/linemanage").setViewName("linemanage");
        registry.addViewController("/web/line/add").setViewName("addline");
        registry.addViewController("/web/lineinfor").setViewName("lineinfor");
        registry.addViewController("/web/driver").setViewName("driverinfor");
        registry.addViewController("/web/add/driver").setViewName("adddriver");
        //registry.addViewController("/web/driver/exam").setViewName("examdriver");
        registry.addViewController("/web/staff").setViewName("staffinfor");
        registry.addViewController("/web/add/staff").setViewName("addstaff");
        registry.addViewController("/web/recruit").setViewName("recruit");
        registry.addViewController("/web/refund").setViewName("refund");
        registry.addViewController("/web/view/hello").setViewName("hello");
        registry.addViewController("/web/message").setViewName("message");
        registry.addViewController("/web/mail").setViewName("mail");
        registry.addViewController("/web/passenger").setViewName("passenger");
        registry.addViewController("/web/add/message").setViewName("addmessage");

    }
}
