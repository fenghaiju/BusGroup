$(document).ready(function () {


    $(document).on("click", ".btn.btn-primary.map", function () {
        $("#modal-body").show();

        var map = new BMap.Map("modal-body");

        var lng=parseFloat($(this).data("lng")) ;
        var lat= parseFloat($(this).data("lat"));
        //$("#myModalLabel").text("东西呢");
        var point=  new BMap.Point(lng,lat);
        map.centerAndZoom(point, 11);

        //缩放控件
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
        map.addControl(top_left_control);
        map.addControl(top_left_navigation);

        //标识动画
        var marker = new BMap.Marker(point);
        map.addOverlay(marker);
        marker.setAnimation(BMAP_ANIMATION_BOUNCE);


        map.enableScrollWheelZoom(true);

    })

})



