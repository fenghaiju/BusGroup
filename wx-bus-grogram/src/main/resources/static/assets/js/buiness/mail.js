$(document).ready(function () {
    //没有更多数据
    var noneleft = "<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing = "<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    $('div#content').empty();
    var body={startNum:1,num:10};
    $.ajax({
        type: 'POST',
        url: '/web/search/message',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var more="<div class='panel panel-success more-flag0'>"+
                "<div class='panel-heading'>" +
                "<center>" +
                "<h3 class='panel-title'>点击加载更多数据</h3>" +
                "</center>" +
                "</div>" +
                "</div>";
            var list="";
            $(res.data).each(function (index,item) {
                list+="<a href='#' class='list-group-item'>" +
                    //"<a href='/web/details/message/"+item.id+"' class='list-group-item'>" +
                    "<h4 class='list-group-item-heading'>" +item.title+"</h4>" +
                    "<p class='list-group-item-text'>发送人:"+item.sender+"&nbsp;&nbsp;&nbsp;&nbsp;发送日期:"+item.sendtime+
                    "<p class='list-group-item-text'>"+item.content+"</p>" +
                    "</a>";

            })
            $("div#content").append(list);
            if(res.data.length<10){
                $("div#content").append(noneleft);
            }else{
                $("div#content").append(more);
            }
        }
    })

})