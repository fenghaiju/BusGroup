$(document).ready(function () {

    //没有更多数据
    var noneleft="<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing="<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";



    //初次加载 审核司机
    $('div.content.table-responsive.table-full-width').empty();
    var page=1;
    var body={startNum:page,num:10};
    $('input[type=hidden]').attr("value",page);

    $.ajax({
        type: 'POST',
        url: '/web/search/searchwaitcheckdriver',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            if(res.data.length<=0){
                $('div.content.table-responsive.table-full-width').html(nothing);
            }else {
                var more="<div class='panel panel-success more-flag0'>"+
                    "<div class='panel-heading'>" +
                    "<center>" +
                    "<h3 class='panel-title'>点击加载更多数据</h3>" +
                    "</center>" +
                    "</div>" +
                    "</div>";
                var thead="<table class='table table-striped'>" +
                    "<thead>" +
                    "<th>账号</th>" +
                    "<th>身份证号码</th>" +
                    "<th>姓名</th>" +
                    "<th>性别</th>" +
                    "<th>国籍</th>" +
                    "<th>住址</th>" +
                    "<th>出生日期</th>" +
                    "<th>手机</th>" +
                    "<th>准驾车型</th>" +
                    "<th>司机类型</th>" +
                    "<th>状态</th>" +
                    "<th>信誉积分</th>"+
                    "<th>操作</th>" +
                    "</thead>"+
                    "<tbody>" +
                    "</tbody>"+
                    "</table>";;
                $('div.content.table-responsive.table-full-width').html(thead);
                var tbody="";
                $(res.data).each(function (index,item) {
                    tbody+="<tr id='"+item.driverId+"'>" +
                        "<td>"+item.driverId+"</td>" +
                        "<td>"+item.driverCitizenship+"</td>" +
                        "<td>"+item.driverName+"</td>" +
                        "<td>"+item.driverGender+"</td>" +
                        "<td>"+item.driverNationality+"</td>" +
                        "<td>"+item.driverAddress+"</td>" +
                        "<td>"+item.birthday+"</td>" +
                        "<td>"+item.driverMobile+"</td>" +
                        "<td>"+item.drivingType+"</td>" +
                        "<td>"+item.driverMark+"</td>" +
                        "<td>"+item.driverStatus+"</td>" +
                        "<td>"+item.integral+"</td>" +
                        "<td><button class='btn exam-p' data-driverid='"+item.driverId+"' data-status='0'>通过</button><button class='btn exam-n' data-driverid='"+item.driverId+"' data-status='3'>不通过</button></td>" +
                        "</tr>";
                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }else{
                    $('div.content.table-responsive.table-full-width').append(more);
                }
            }
        }

    });



    //flag_0点击 再次点击审核司机信息
    $('li.flag_0').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class","active flag_0");
        $('li.flag_1').attr("class","flag_1");
        $('li.flag_2').attr("class","flag_2");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);

        $.ajax({
            type: 'POST',
            url: '/web/search/searchwaitcheckdriver',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(body),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag0'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var thead="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>账号</th>" +
                        "<th>身份证号码</th>" +
                        "<th>姓名</th>" +
                        "<th>性别</th>" +
                        "<th>国籍</th>" +
                        "<th>住址</th>" +
                        "<th>出生日期</th>" +
                        "<th>手机</th>" +
                        "<th>准驾车型</th>" +
                        "<th>司机类型</th>" +
                        "<th>状态</th>" +
                        "<th>信誉积分</th>"+
                        "<th>操作</th>" +
                        "</thead>"+
                        "<tbody>" +
                        "</tbody>"+
                        "</table>";
                    $('div.content.table-responsive.table-full-width').html(thead);
                    var tbody="";
                    $(res.data).each(function (index,item) {
                        tbody+="<tr id='"+item.driverId+"'>" +
                            "<td>"+item.driverId+"</td>" +
                            "<td>"+item.driverCitizenship+"</td>" +
                            "<td>"+item.driverName+"</td>" +
                            "<td>"+item.driverGender+"</td>" +
                            "<td>"+item.driverNationality+"</td>" +
                            "<td>"+item.driverAddress+"</td>" +
                            "<td>"+item.birthday+"</td>" +
                            "<td>"+item.driverMobile+"</td>" +
                            "<td>"+item.drivingType+"</td>" +
                            "<td>"+item.driverMark+"</td>" +
                            "<td>"+item.driverStatus+"</td>" +
                            "<td>"+item.integral+"</td>" +
                            "<td><button class='btn exam-p'  data-driverid='"+item.driverId+"' data-status='0'>通过</button><button class='btn exam-n' data-driverid='"+item.driverId+"' data-status='3'>不通过</button></td>" +
                            "</tr>";
                    })
                    $('tbody').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        });
    });


    //审核司机加载更多  flag=0
    $(document).on("click","div.panel.panel-success.more-flag0",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/searchwaitcheckdriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index,item) {
                    tbody+="<tr id='"+item.driverId+"'>" +
                        "<td>"+item.driverId+"</td>" +
                        "<td>"+item.driverCitizenship+"</td>" +
                        "<td>"+item.driverName+"</td>" +
                        "<td>"+item.driverGender+"</td>" +
                        "<td>"+item.driverNationality+"</td>" +
                        "<td>"+item.driverAddress+"</td>" +
                        "<td>"+item.birthday+"</td>" +
                        "<td>"+item.driverMobile+"</td>" +
                        "<td>"+item.drivingType+"</td>" +
                        "<td>"+item.driverMark+"</td>" +
                        "<td>"+item.driverStatus+"</td>" +
                        "<td>"+item.integral+"</td>" +
                        "<td><button class='btn exam-p' data-driverid='"+item.driverId+"' data-status='0'>通过</button><button class='btn exam-n' data-driverid='"+item.driverId+"' data-status='3'>不通过</button></td>" +
                        "</tr>";

                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }

            }

        });

    });

    //--------------------------以下为可用司机-----------------------------

    //flag_1点击 点击可用司机
    $('li.flag_1').click(function () {
        $('div.content.table-responsive.table-full-width').empty();
        $('li.flag_1').attr("class","active flag_1");
        $('li.flag_0').attr("class","flag_0");
        $('li.flag_2').attr("class","flag_2");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/searchcanusedriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag1'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var thead="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>账号</th>" +
                        "<th>身份证号码</th>" +
                        "<th>姓名</th>" +
                        "<th>性别</th>" +
                        "<th>国籍</th>" +
                        "<th>住址</th>" +
                        "<th>出生日期</th>" +
                        "<th>手机</th>" +
                        "<th>准驾车型</th>" +
                        "<th>司机类型</th>" +
                        "<th>状态</th>" +
                        "<th>信誉积分</th>"+
                        "<th>操作</th>" +
                        "</thead>"+
                        "<tbody>" +
                        "</tbody>"+
                        "</table>";
                    $('div.content.table-responsive.table-full-width').html(thead);
                    var tbody="";
                    $(res.data).each(function (index,item) {
                        tbody+="<tr id='"+item.driverId+"'>" +
                            "<td>"+item.driverId+"</td>" +
                            "<td>"+item.driverCitizenship+"</td>" +
                            "<td>"+item.driverName+"</td>" +
                            "<td>"+item.driverGender+"</td>" +
                            "<td>"+item.driverNationality+"</td>" +
                            "<td>"+item.driverAddress+"</td>" +
                            "<td>"+item.birthday+"</td>" +
                            "<td>"+item.driverMobile+"</td>" +
                            "<td>"+item.drivingType+"</td>" +
                            "<td>"+item.driverMark+"</td>" +
                            "<td>"+item.driverStatus+"</td>" +
                            "<td>"+item.integral+"</td>" +
                            "<td><button class='btn exam-p' data-driverid='"+item.driverId+"' data-status='2'>停用</button>" +
                            "</tr>";
                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        })


    });

    //flag1  可用司机加载更多
    $(document).on("click","div.panel.panel-success.more-flag1",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/searchstopusedriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index, item) {
                    tbody+="<tr id='"+item.driverId+"'>" +
                        "<td>"+item.driverId+"</td>" +
                        "<td>"+item.driverCitizenship+"</td>" +
                        "<td>"+item.driverName+"</td>" +
                        "<td>"+item.driverGender+"</td>" +
                        "<td>"+item.driverNationality+"</td>" +
                        "<td>"+item.driverAddress+"</td>" +
                        "<td>"+item.birthday+"</td>" +
                        "<td>"+item.driverMobile+"</td>" +
                        "<td>"+item.drivingType+"</td>" +
                        "<td>"+item.driverMark+"</td>" +
                        "<td>"+item.driverStatus+"</td>" +
                        "<td>"+item.integral+"</td>" +
                        "<td><button class='btn exam-p' data-driverid='"+item.driverId+"' data-status='2'>停用</button>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }
        });

    })
//--------------------------------------以下是不可用司机-----------------------------------
    //flag_2点击 点击不可用司机
    $('li.flag_2').click(function () {
        $('div.content.table-responsive.table-full-width').empty();
        $('li.flag_2').attr("class","active flag_2");
        $('li.flag_0').attr("class","flag_0");
        $('li.flag_1').attr("class","flag_1");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/searchstopusedriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag1'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var thead="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>账号</th>" +
                        "<th>身份证号码</th>" +
                        "<th>姓名</th>" +
                        "<th>性别</th>" +
                        "<th>国籍</th>" +
                        "<th>住址</th>" +
                        "<th>出生日期</th>" +
                        "<th>手机</th>" +
                        "<th>准驾车型</th>" +
                        "<th>司机类型</th>" +
                        "<th>状态</th>" +
                        "<th>信誉积分</th>"+
                        "<th>操作</th>" +
                        "</thead>"+
                        "<tbody>" +
                        "</tbody>"+
                        "</table>";
                    $('div.content.table-responsive.table-full-width').html(thead);
                    var tbody="";
                    $(res.data).each(function (index,item) {
                        tbody+="<tr id='"+item.driverId+"'>" +
                            "<td>"+item.driverId+"</td>" +
                            "<td>"+item.driverCitizenship+"</td>" +
                            "<td>"+item.driverName+"</td>" +
                            "<td>"+item.driverGender+"</td>" +
                            "<td>"+item.driverNationality+"</td>" +
                            "<td>"+item.driverAddress+"</td>" +
                            "<td>"+item.birthday+"</td>" +
                            "<td>"+item.driverMobile+"</td>" +
                            "<td>"+item.drivingType+"</td>" +
                            "<td>"+item.driverMark+"</td>" +
                            "<td>"+item.driverStatus+"</td>" +
                            "<td>"+item.integral+"</td>" +
                            "<td><button class='btn exam-p' data-driverid='"+item.driverId+"' data-status='0'>启用</button>" +
                            "</tr>";
                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        })


    });

    //flag1  不可用司机加载更多
    $(document).on("click","div.panel.panel-success.more-flag2",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/searchstopusedriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index, item) {
                    tbody+="<tr id='"+item.driverId+"'>" +
                        "<td>"+item.driverId+"</td>" +
                        "<td>"+item.driverCitizenship+"</td>" +
                        "<td>"+item.driverName+"</td>" +
                        "<td>"+item.driverGender+"</td>" +
                        "<td>"+item.driverNationality+"</td>" +
                        "<td>"+item.driverAddress+"</td>" +
                        "<td>"+item.birthday+"</td>" +
                        "<td>"+item.driverMobile+"</td>" +
                        "<td>"+item.drivingType+"</td>" +
                        "<td>"+item.driverMark+"</td>" +
                        "<td>"+item.driverStatus+"</td>" +
                        "<td>"+item.integral+"</td>" +
                        "<td><button class='btn exam-p' data-driverid='"+item.driverId+"' data-status='0'>启用</button>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }
        });

    })

    //司机状态改变
    $(document).on("click","button",function () {
        var jsonObj={};
        jsonObj.driverStatus=$(this).data("status");
        jsonObj.driverId=$(this).data("driverid");

        $.ajax({
            type: 'POST',
            url: '/web/operate/driveroperate',
            contentType: "application/json;charset=utf-8",
            DataType: "json",
            data: JSON.stringify(jsonObj),
            error: function () {
                alert("加载失败，请刷新重试！");
            },
            success: function (res) {
                if(res.errno==0){
                    $("tr#"+jsonObj.driverId).remove();
                }
            }
        })
    })


})